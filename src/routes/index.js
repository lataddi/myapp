const express = require('express')
const router = express.Router()

// Se muestra la plantilla
router.get('/', (req, res) => {
    res.render('index', { title: 'Inicio'});
});

module.exports = router;