const express = require('express')
const path = require('path')
const app = express();


// Se indica el directorio donde se almacenarán las plantillas 
app.set('views', path.join(__dirname, 'views'));
// Se indica el motor del plantillas a utilizar
app.set('view engine', 'pug');

//routes
app.use(require('./routes/index'))

app.use(express.static(path.join(__dirname, 'public')));

app.listen(8001, () => {
    console.log('Este servidor escucha por el puerto 8001');
})